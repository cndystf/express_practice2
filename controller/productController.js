import Products  from '../resources/products.js';

export const list = (req, res) => {
  const products = Products.list();
  return res.status(200).json(products);
}

export const create = (req, res) => {
  const product = Products.create(req.body);
  return res.status(201).json(product);
}

export const find = (req, res) => {
  const product = Products.find(req.params.id);
  return res.status(200).json(product);
}

export const remove = (req, res) => {
  const isRemoved = Products.remove(req.params.id);
  return res.status(200).send('Delete Successful');
}


//old vers
// export const get = (req, res) => {
//     return res.send('list product')
// }

// export const create = (req, res) => {
//     return res.send('create product')
// }

// export const update = (req, res) => {
//     return res.send('update product')
// }

// export const destroy = (req, res) => {
//     return res.send('delete product')
// }

// export const find = (req, res) => {
//     return res.send('find product')
// }