import Chart from '../resources/chart.js';

export const list = (req, res) => {
  const charts = Chart.list();
  return res.status(200).json(charts);
}

export const create = (req, res) => {
  const chart = Chart.create(req.body);
  return res.status(201).json(chart);
}

export const find = (req, res) => {
  const chart = Chart.find(req.params.id);
  return res.status(200).json(chart);
}

export const remove = (req, res) => {
  const isRemoved = Chart.remove(req.params.id);
  return res.status(200).send('Delete Successful');
}
