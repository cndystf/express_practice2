let products = [];

const Products = {
  
  list() {
    return products;
  },

  create(params) {
    const product = {
      id: params.id,
      name: params.name,
      desc: params.desc
    };
    products.push(product);
    return product;
  },

  find(id) {
    const product = products.find((i) => i.id === Number(id));
    if(!product) {
      return null;
    }
    return product;
  },

  remove(id) {
    const product = products.find((i) => i.id === Number(id));
    if(!product) {
      return null;
    }
    products = products.filter((i) => i.id !== Number(id));
    return true;
  }
}

export default Products;
