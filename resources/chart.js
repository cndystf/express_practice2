let charts = [];

const Chart = {
  
  list() {
    return charts;
  },

  create(params) {
    const chart = {
      id: params.id,
      name: params.name,
      desc: params.desc
    };
    charts.push(chart);
    return chart;
  },

  find(id) {
    const chart = charts.find((i) => i.id === Number(id));
    if(!chart) {
      return null;
    }
    return chart;
  },

  remove(id) {
    const chart = charts.find((i) => i.id === Number(id));
    if(!chart) {
      return null;
    }
    charts = charts.filter((i) => i.id !== Number(id));
    return true;
  }
}

export default Chart;
