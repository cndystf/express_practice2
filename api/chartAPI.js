import express from 'express';
import { list, create, find, remove } from '../controller/chartController.js';

const routes = express.Router();

routes.get('/list', list);
routes.post('/create', create);
routes.get('/:id', find);
routes.get('/remove/:id', remove)

export default routes;
